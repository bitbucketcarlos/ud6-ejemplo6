# Ud6-Ejemplo6
_Ejemplo 6 de la Unidad 6._

Vamos a implementar una aplicación en la que haremos uso tanto de _PreferenceFragment_ como de _SharedPreferences_. La aplicación 
consistirá en cambiar a modo noche según las preferencias guardadas por el usuario. 

## Paso 1: Añadir dependencias al _gradle_

El primer paso será añadir al fichero _build.gradle(Module:app)_ la dependencia _implementation 'androidx.preference:preference:1.1.1'_ 
para poder hacer uso de las preferencias.

## Paso 2: Creación del fichero _preferencias.xml_

En él añadiremos los pares de clave/valor que queremos guardar. en este caso de tipo _SwitchPreference_.

```html
<PreferenceScreen xmlns:app="http://schemas.android.com/apk/res-auto">
    <SwitchPreference
        app:key="ModoNoche"
        app:title="Modo noche"
        app:defaultValue="false"
        app:summaryOn="@string/activado"
        app:summaryOff="@string/desactivado"/>
</PreferenceScreen>
```

## Paso 3: Creación de la clase _PreferenciasFragment_

En ella añadiremos las preferencias creadas en el fichero _preferencias.xml_. Ésta heredará de _PreferenceFragmentCompat_.

```java
public class PreferenciasFragment extends PreferenceFragmentCompat  {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Añadimos las preferencias creadas en el fichero "preferencias.xml"
        addPreferencesFromResource(R.xml.preferencias);
    }
}
```

## Paso 4: Creación de la actividad _ActividadPreferencias_

Será la encargada de mostrar el _Fragment_ anterior y de cambiar el modo. Además, comprobaremos si se pulsa el botón de _Atrás_.

```java
public class ActividadPreferencias extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creamos la transacción y añadimos el PreferenceFragment creado
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }

    // Método para cambiar el modo
    public void CambiarModo(){
        // Obtenemos las preferencias creadas en el fichero "preferencias.xml"
        SharedPreferences prefe = PreferenceManager.getDefaultSharedPreferences(ActividadPreferencias.this);

        // Obtenemos el valor del SwitchPreference
        boolean modoNoche = prefe.getBoolean("ModoNoche", true);

        if(modoNoche)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

    }

    // Si se pulsa el botón de "Atrás" cambiamos el modo.
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        CambiarModo();
    }
}
```

## Paso 5: Creación de la actividad _MainActivity_
En ella mostraremos el menú de opciones y cambiaremos el modo si pulsamos sobre el botón de regreso o el de _Atrás_ desde la actividad _ActividadPreferencias_.

### _activity_main.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/fondo"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/texto"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/hello_world"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

### _menu.xml_
```html
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">
    <item android:id="@+id/preferencias"
        android:title="@string/preferencias"
        app:showAsAction="never">
    </item>
</menu>
```

### _MainActivity.java_
```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtenermos las preferencias almacenadas
        SharedPreferences prefe = PreferenceManager.getDefaultSharedPreferences(this);
        boolean modoNoche = prefe.getBoolean("ModoNoche", false);

        if(modoNoche)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // "Inflamos" el menú diseñado
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Si se pulsa sobre el item "Preferencias" abrimos la actividad ActividadPreferencias
        if(item.getItemId() == R.id.preferencias){
            Intent intentPref = new Intent(this, ActividadPreferencias.class);
            startActivity(intentPref);

            return true;
        }
        return super.onContextItemSelected(item);
    }
}
```