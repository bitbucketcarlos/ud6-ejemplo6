package com.example.ud6_ejemplo6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.os.Bundle;

public class ActividadPreferencias extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creamos la transacción y añadimos el PreferenceFragment creado
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }

    // Método para cambiar el modo
    public void CambiarModo(){
        // Obtenemos las preferencias creadas en el fichero "preferencias.xml"
        SharedPreferences prefe = PreferenceManager.getDefaultSharedPreferences(ActividadPreferencias.this);

        // Obtenemos el valor del SwitchPreference
        boolean modoNoche = prefe.getBoolean("ModoNoche", true);

        if(modoNoche)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

    }

    // Si se pulsa el botón de "Atrás" cambiamos el modo.
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        CambiarModo();
    }
}